#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define MAX 80
#define MAXIT 10
#define ALE 10

// Inicializamos el tablero
int tablero[MAX][MAX] = {[0 ... MAX-1][0 ... MAX-1] = 0};
int tablero2[MAX][MAX] = {[0 ... MAX-1][0 ... MAX-1] = 0};
int final[MAX][MAX] = {[0 ... MAX-1][0 ... MAX-1] = 0};
int tablero_tmp[MAX][MAX] = {[0 ... MAX-1][0 ... MAX-1] = 0};

//int tablero[MAX][MAX];
//int tablero2[MAX][MAX];
//int tablero_tmp[MAX][MAX];

int rank, size;
int tag = 123;
int  namelen;
char processor_name[MPI_MAX_PROCESSOR_NAME];

// Genera un tablero aleatorio
void aleatorio () {
	int i, j;
	int valor;
	for (i = 1; i < MAX - 1; i++) {
		for (j = 1; j < MAX - 1; j++) {
			valor =  rand() % ALE;
			if (valor == 0)  {
				tablero[i][j] = 1;
			}
		}
	}

}

// Pintamos el tablero
void pinta (int op) {
	int i, j;
	for (i = 1; i < MAX - 1; i++) {
		for (j = 1; j < MAX - 1; j++) {
//			if (!op) {
				printf("%d", tablero[i][j]);
/*			}
			else {
				printf("%d", final[i][j]);
			}*/
		}
		printf("\n");
	}
}

// Cuenta vecinas a una casilla
int cuenta (int i, int j) {
	int vecinas;
	vecinas = tablero2[i - 1][j - 1];
	vecinas += tablero2[i][j - 1];
	vecinas += tablero2[i + 1][j - 1];
	vecinas += tablero2[i - 1][j];
	vecinas += tablero2[i + 1][j];
	vecinas += tablero2[i - 1][j + 1];
	vecinas += tablero2[i][j + 1];
	vecinas += tablero2[i + 1][j + 1];
//	printf("vecinas: %d [%d,%d]\n", vecinas,i,j);
	return vecinas;
}


// Movimiento de la vida
void mueve () {
	int i, j;
	int vecinas;
	int tam = sizeof(tablero)/size;
	int tam_tab = sizeof(tablero);
	int cacho = MAX/size;


	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Scatter(&tablero,sizeof(tablero)/size,MPI_INT,&tablero2,sizeof(tablero)/size,MPI_INT,0,MPI_COMM_WORLD);		
//	printf("\nTEMPORAL\n");
	for (i = 0; i < cacho; i++) {
		for (j = 0; j < MAX; j++) {
//			printf("%d",tablero[i][j]);
			vecinas = cuenta(i,j);
			if (vecinas == 3) {
				tablero_tmp[i][j] = 1;
			}
			else if ((vecinas == 2) && (tablero2[i][j] == 1)) {
				tablero_tmp[i][j] = 1;
			}
			else {
				tablero_tmp[i][j] = 0;
			}
		}
//		printf("\n");
	}
	for (i = 0; i < cacho; i++) {
		for (j = 0; j < MAX; j++) {
			tablero2[i][j] = tablero_tmp[i][j];
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);
//	MPI_Gather(&tablero_tmp,tam,MPI_INT,&tablero,tam_tab,MPI_INT,0,MPI_COMM_WORLD);		
	MPI_Gather(&tablero2,sizeof(tablero)/size,MPI_INT,&tablero,sizeof(tablero)/size,MPI_INT,0,MPI_COMM_WORLD);		
}


//int main(int argc, char * argv[]) {
int main (int  argc, char **argv) {	
	MPI_Init( &argc, &argv );
	MPI_Comm_size( MPI_COMM_WORLD, &size );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	MPI_Get_processor_name(processor_name, &namelen);
	MPI_Status status;
	aleatorio();
	if (!rank) {
		pinta(0);
		printf("------------------------------------------\n");
	}

	MPI_Barrier(MPI_COMM_WORLD);

	double inicio, fin;
	int i;

	inicio = MPI_Wtime();
	for (i = 0; i < MAXIT; i++) {
		mueve();

	}
	fin = MPI_Wtime();

	if (!rank) {
		printf("------------------------------------------\n");
		pinta(1);
		printf("\n\n\t:Tiempo TOTAL:\n\t%f segundos\n",(fin-inicio));
	}
	MPI_Finalize();
}



