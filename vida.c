#include <stdio.h>
#include <stdlib.h>

#define MAX 80 
#define MAXIT 8
#define ALE 10

// Inicializamos el tablero
int tablero[MAX][MAX] = {[0 ... MAX-1][0 ... MAX-1] = 0};
int tablero_tmp[MAX][MAX] = {[0 ... MAX-1][0 ... MAX-1] = 0};


// Genera un tablero aleatorio
void aleatorio () {
	int i, j;
	int valor;
	for (i = 1; i < MAX - 1; i++) {
		for (j = 1; j < MAX - 1; j++) {
			valor =  rand() % ALE;
			if (valor == 0)  {
				tablero[i][j] = 1;
			}
		}
	}

}

// Pintamos el tablero
void pinta () {
	int i, j;
	for (i = 1; i < MAX - 1; i++) {
		for (j = 1; j < MAX - 1; j++) {
			printf("%d", tablero[i][j]);
		}
		printf("\n");
	}
}

// Cuenta vecinas a una casilla
int cuenta (int i, int j) {
	int vecinas;
	vecinas = tablero[i - 1][j - 1];
	vecinas += tablero[i][j - 1];
	vecinas += tablero[i + 1][j - 1];
	vecinas += tablero[i - 1][j];
	vecinas += tablero[i + 1][j];
	vecinas += tablero[i - 1][j + 1];
	vecinas += tablero[i][j + 1];
	vecinas += tablero[i + 1][j + 1];
	return vecinas;
}


// Movimiento de la vida
void mueve () {
	int i, j;
	int vecinas;
	for (i = 1; i < MAX - 1; i++) {
		for (j = 1; j < MAX - 1; j++) {
			vecinas = cuenta(i,j);
			if (vecinas == 3) {
				tablero_tmp[i][j] = 1;
			}
			else if ((vecinas == 2) && (tablero[i][j] == 1)) {
				tablero_tmp[i][j] = 1;
			}
			else {
				tablero_tmp[i][j] = 0;
			}
		}
	}
	for (i = 1; i < MAX - 1; i++) {
		for (j = 1; j < MAX - 1; j++) {
			tablero[i][j] = tablero_tmp[i][j];
		}
	}
}


//int main(int argc, char * argv[]) {
int main () {
	aleatorio();
	pinta();
	int i;
	for (i = 0; i < MAXIT; i++) {
		mueve();
	}
	printf("------------------------------------------\n");
	pinta();
}



